package fr.badcookie20.comm.sample;

import fr.badcookie20.comm.client.ConnectionHandler;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("mainwindow.fxml"));
        Scene scene = new Scene(root, 550, 235);

        initMenu();

        primaryStage.setScene(scene);
        primaryStage.setTitle("Instant Messaging");
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    private MenuBar initMenu() {
        MenuBar bar = new MenuBar();
        bar.getMenus().addAll(getConnectionMenu(), getServerMenu());
        return bar;
    }

    public Menu getConnectionMenu() {
        Menu menuConnection = new Menu();

        MenuItem connect = new MenuItem("Connexion à un serveur");
        connect.setOnAction(t -> {
            // TODO
        });

        MenuItem disconnect = new MenuItem("Déconnexion");
        disconnect.setOnAction(t -> {
            // TODO
        });

        MenuItem exit = new MenuItem("Quitter");
        exit.setOnAction(t -> {
            // TODO
        });

        menuConnection.getItems().addAll(connect, disconnect, exit);

        return menuConnection;
    }

    public Menu getServerMenu() {
        Menu menuServer = new Menu();

        MenuItem changeUsername = new MenuItem("Changer de pseudo");
        changeUsername.setOnAction(t -> {
            // TODO
        });

        MenuItem infos = new MenuItem("Informations sur le serveur");
        infos.setOnAction(t -> {
            // TODO
        });

        menuServer.getItems().addAll(changeUsername, infos);

        return menuServer;
    }


    public static void main(String[] args) {
        launch(args);
    }
}
