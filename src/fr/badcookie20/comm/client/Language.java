package fr.badcookie20.comm.client;

public enum Language {

    FRENCH("français", "fr", "french", "francés"),
    ENGLISH("anglais", "en", "english", "inglés"),
    SPANISH("espagnol", "es", "español", "spanish");

    private String[] names;

    Language(String... names) {
        this.names = names;
    }

    public static Language getLanguage(String language) {
        for(Language l : values()) {
            for(String name : l.names) {
                if(name.equalsIgnoreCase(language)) {
                    return l;
                }
            }
        }

        return null;
    }
}
