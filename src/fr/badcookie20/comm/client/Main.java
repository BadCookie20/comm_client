package fr.badcookie20.comm.client;

import javax.swing.*;
import java.awt.*;

public class Main {

    public static final int PORT = 6154;
    public static final String VERSION = "Alpha 1.4";

    public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");

        try {
            Window client = new Window();
        }catch(Throwable e) {
            if(ConnectionHandler.getInstance() == null) {
                Window.getInstance().sayUnhandledError();
            }else{
                ConnectionHandler.getInstance().forceDisconnect();
            }
        }
    }

}
