package fr.badcookie20.comm.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;

public class ConnectionHandler {

    private static ConnectionHandler instance;

    public static void init(String ip, String username, int port) {
        new ConnectionHandler(ip, username, port);
    }

    public static void init() {
        String[] data = Window.getInstance().askConn();
        int port = Main.PORT;

        init(data[0], data[1], port);
    }

    private ObjectInputStream ois;
    private ObjectOutputStream oos;
    private Socket s;
    private String ip;

    private boolean connected;

    private ConnectionHandler(String ip, String username, int port) {
        if(ip == null) {
            return;
        }

        try {
            this.s = new Socket(ip, port);
            this.oos = new ObjectOutputStream(s.getOutputStream());
            this.ois = new ObjectInputStream(s.getInputStream());
            this.ip = ip;
            Window.getInstance().append("Connecté au serveur " + this.ip);
        }catch(IOException e) {
            Window.getInstance().sayCouldntConnect(ip);
            return;
        }

        instance = this;

        if(!preConnection(username)) {
            disconnect(false);
            return;
        }
        Thread t = new Thread() {
            @Override
            public void run() {
                chat();
                disconnect(false);
            }
        };
        t.start();
    }

    public boolean preConnection(String nickname) {
        try {
            String version = (String) ois.readObject();
            if(!version.equals(Main.VERSION)) {
                Window.getInstance().sayIncorrectVersion(version, this.ip);
                return false;
            }

            if(nickname == null) {
                this.forceDisconnect();
                return false;
            }

            oos.writeObject(nickname);
            Window.getInstance().enableSending();
            Window.getInstance().updateTitle(this.ip);
            Window.getInstance().setConnected();
            connected = true;
            return true;
        }catch(IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void chat() {
        String comingObject;
        try {
            do {
                comingObject = (String) ois.readObject();
                if (comingObject.startsWith("nicknamealreadyused:")) {
                    Window.getInstance().sayAlreadyUsedNickname(comingObject.split(":")[1]);
                    break;
                } else if (comingObject.startsWith("newnickname:")) {
                    Window.getInstance().append("Vous avez changé votre pseudo pour " + comingObject.split(":")[1]);
                } else if (comingObject.equals("disconnect")) {
                    break;
                }else if(comingObject.startsWith("data:")) {
                    handleData(comingObject);
                } else {
                    Window.getInstance().append(comingObject);
                    Window.getInstance().addNotification(comingObject);
                }
            } while (!comingObject.equals("disconnect") || !s.isClosed());
            connected = false;
        }catch(SocketException ignored) {
            connected = false;
        }catch(IOException | ClassNotFoundException ignored) {
            Window.getInstance().sayConnectionClosedError(this.ip);
        }
    }

    private void handleData(String comingObject) {
        String data = comingObject.split(":")[1];
        String[] dataArray = data.split(",");
        String finalString = "<html><body>";
        finalString+="IP du serveur : " + this.ip + "<br>";

        String tempUsers = "";
        String users = dataArray[1];
        String[] usersArray = users.split(";");
        int i = 0;
        for(String user : usersArray) {
            i++;
            if(i % 5 == 0) {
                tempUsers += "<br>" + user;
            }else {
                tempUsers += user;
            }
        }

        finalString += "Utilisateurs connectés : " + dataArray[0] + " (" + tempUsers + ")";
        finalString += "</body></html>";
        Window.getInstance().sayServerInfo(finalString);
    }

    public void sendMessage(String text) {
        if(text.isEmpty()) {
            Window.getInstance().append("Le message est vide !");
            return;
        }

        if(this.s.isClosed()) {
            Window.getInstance().sayConnectionClosedError(this.ip);
            this.forceDisconnect();
        }

        try {
            if(text.equals("/disconnect")) {
                this.disconnect(false);
                return;
            }

            oos.writeObject(text);
            oos.flush();
            Window.getInstance().clearMessageField();
        } catch (Exception e) {
            Window.getInstance().append("Le message ne peut pas être envoyé...");
        }
    }

    public void disconnect(boolean quit) {
        if(!connected && quit) System.exit(0);
        if(!connected) return;
        Window.getInstance().disableSending();

        try {
            oos.writeObject("disconnected");
            oos.close();
            ois.close();
            s.close();
            Window.getInstance().sayConnectionClosed(this.ip, quit);
        } catch (IOException ignored) {
            Window.getInstance().sayConnectionClosedError(this.ip);
        }

        Window.getInstance().setDisconnected();
        connected = false;
        instance = null;
    }

    public void forceDisconnect() {
        connected = true;
        disconnect(false);
        connected = false;
    }

    public static ConnectionHandler getInstance() {
        return instance;
    }

    public static void emptyInstance() {
        instance = null;
    }

    public static boolean isConnected() {
        return instance != null;
    }

}
