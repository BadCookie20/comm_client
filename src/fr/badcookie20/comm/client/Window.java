package fr.badcookie20.comm.client;

import javafx.scene.control.*;

import java.awt.*;
import java.awt.MenuItem;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class Window extends JFrame {

    private static final long serialVersionUID = 1444945881240464337L;
    private static Window instance;
    private static final String NO_CONN = "Instant Messaging - (aucun serveur)";
    private static final String IP = "Instant Messaging - ";

    private TrayIcon trayIcon;
    private boolean isNotifSupported;

    protected JTextArea clientDisplay;

    private JTextField messageField;
    private JButton send;

    private JMenuBar bar;
    private JMenu menuConnection;
    private JMenu menuServer;
    private JMenuItem changeUsername;
    private JMenuItem disconnect;
    private JMenuItem connect;
    private JMenuItem serverInfos;
    private JMenuItem exit;

    public Window() {
        super("Instant Messaging - (aucun serveur)");

        this.messageField = new JTextField();
        this.messageField.setEditable(false);
        this.messageField.addActionListener( e -> ConnectionHandler.getInstance().sendMessage(e.getActionCommand()));
        this.messageField.setColumns(45);

        this.send = new JButton("Envoyer");
        this.send.setEnabled(false);
        this.send.addActionListener( e -> ConnectionHandler.getInstance().sendMessage(messageField.getText()));

        this.clientDisplay = new JTextArea();
        this.clientDisplay.setEditable(false);

        this.bar = new JMenuBar();
        this.menuConnection = new JMenu("Connexion");
        this.menuServer = new JMenu("Serveur");
        this.bar.add(menuConnection);
        this.bar.add(menuServer);

        this.changeUsername = new JMenuItem("Changer de pseudo");
        this.changeUsername.addActionListener(e -> askNewUsername());
        this.changeUsername.getAccessibleContext().setAccessibleDescription("Permet de changer son nom d'utilisateur dans la mesure où il n'est pas déjà utilisé");
        this.changeUsername.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, KeyEvent.ALT_MASK));

        this.serverInfos = new JMenuItem("Informations sur le serveur");
        this.serverInfos.addActionListener(e -> ConnectionHandler.getInstance().sendMessage("getdata"));

        this.connect = new JMenuItem("Connexion à un serveur");
        this.connect.addActionListener(e -> {
            String[] data = askConn();
            int port = Main.PORT;
            ConnectionHandler.init(data[0], data[1], port);
        });
        this.connect.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.ALT_MASK));

        this.disconnect = new JMenuItem("Se déconnecter");
        this.disconnect.addActionListener(e -> ConnectionHandler.getInstance().disconnect(false));
        this.disconnect.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, KeyEvent.ALT_MASK));

        this.exit = new JMenuItem("Quitter");
        this.exit.addActionListener( e -> quit());
        this.exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.ALT_MASK));

        this.menuServer.add(changeUsername);
        this.menuServer.add(serverInfos);
        this.menuConnection.add(connect);
        this.menuConnection.add(disconnect);
        this.menuConnection.add(exit);

        this.setJMenuBar(bar);

        Container content = this.getContentPane();
        content.setLayout(new BorderLayout());

        JPanel north = new JPanel();
        north.add(send, FlowLayout.LEFT);
        north.add(messageField, FlowLayout.LEFT);
        content.add(north, BorderLayout.NORTH);
        content.add(new JScrollPane(clientDisplay), BorderLayout.CENTER);

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setSize(500, 300);
        this.setResizable(false);

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);

        setDisconnected();

        this.setVisible(true);
        instance = this;

        this.isNotifSupported = SystemTray.isSupported();

        if(!isNotifSupported) {
            this.sayUnsupportedSystemTray();
        }else{
            initNotifs();
        }
    }

    private void initNotifs() {
        SystemTray t = SystemTray.getSystemTray();
        this.trayIcon = new TrayIcon(createImage(10, 10), "Instant Messaging");

        PopupMenu menu = new PopupMenu();
        MenuItem focus = new MenuItem("Afficher la fenêtre");
        MenuItem quit = new MenuItem("Quitter");

        focus.addActionListener(e -> {
            setVisible(false);
            setVisible(true);
        });
        quit.addActionListener(e -> quit());

        menu.add(focus);
        menu.addSeparator();
        menu.add(quit);

        this.trayIcon.setPopupMenu(menu);

        try {
            t.add(trayIcon);
        }catch(AWTException e) {
            sayUnsupportedSystemTray();
        }
    }

    public void disableSending() {
        this.messageField.setEnabled(false);
        this.send.setEnabled(false);
    }

    public void enableSending() {
        this.messageField.setEnabled(true);
        this.messageField.setEditable(true);
        this.send.setEnabled(true);
    }

    public String[] askConn() {
        JPanel p = new JPanel();
        JLabel label = new JLabel("Entrez l'adresse du serveur :");
        JLabel label2 = new JLabel("Entrez votre pseudo :");
        JTextField f1 = new JTextField(15);
        JTextField f2 = new JTextField(15);
        p.add(label);
        p.add(f1);
        p.add(label2);
        p.add(f2);
        JOptionPane.showMessageDialog(null, p, "Instant Messaging", JOptionPane.QUESTION_MESSAGE);
        String ip = f1.getText();
        String username = f2.getText();
        if(ip == null || ip.isEmpty()) {
            return null;
        }

        return new String[] {ip, username};
    }

    public String askUsername() {
        JPanel p = new JPanel();
        JLabel label = new JLabel("Entrez votre pseudo :");
        p.add(label);
        String tempNickame = JOptionPane.showInputDialog(null, p, "Instant Messaging", JOptionPane.QUESTION_MESSAGE);
        if(tempNickame == null || tempNickame.isEmpty()) {
            return null;
        }

        return tempNickame;
    }

    private String askNewUsername() {
        JPanel p = new JPanel();
        JLabel label = new JLabel("Nouveau pseudonyme :");
        p.add(label);
        String tempNickame = JOptionPane.showInputDialog(null, p, "Instant Messaging", JOptionPane.QUESTION_MESSAGE);
        if(tempNickame == null || tempNickame.isEmpty()) {
            sayEmptyUsername();
            return "";
        }
        ConnectionHandler.getInstance().sendMessage("/nickname " + tempNickame);
        return tempNickame;
    }

    private void sayEmptyUsername() {
        JPanel p = new JPanel();
        JLabel label = new JLabel("Le pseudo spécifié est vide !");
        p.add(label);
        JOptionPane.showMessageDialog(null, p, "Instant Messaging", JOptionPane.WARNING_MESSAGE);
        ConnectionHandler.emptyInstance();
    }

    public void sayCouldntConnect(String ip) {
        JPanel p = new JPanel();
        JLabel label = new JLabel("La connexion avec le serveur " + ip + " n'a pas pu être établie");
        p.add(label);
        JOptionPane.showMessageDialog(null, p, "Instant Messaging", JOptionPane.ERROR_MESSAGE);
        ConnectionHandler.emptyInstance();
    }

    public void sayConnectionClosed(String ip, boolean dispose) {
        Window.getInstance().append("Deconnecté de " + ip);
        JPanel p = new JPanel();
        JLabel label = new JLabel("La connexion avec le serveur " + ip + " est fermée !");
        p.add(label);
        JOptionPane.showMessageDialog(null, p, "Instant Messaging", JOptionPane.INFORMATION_MESSAGE);
        if(dispose) System.exit(0);
    }

    public void sayConnectionClosedError(String ip) {
        Window.getInstance().append("Deconnecté de " + ip + " d'une manière inattendue");
        JPanel p = new JPanel();
        JLabel label = new JLabel("<html><body>La connexion au serveur " + ip + " a été fermée d'une manière inatendue ! <br>Veuillez vérifier l'état du réseau ainsi que celle du serveur</body></html>");
        p.add(label);
        JOptionPane.showMessageDialog(null, p, "Instant Messaging", JOptionPane.ERROR_MESSAGE);
    }

    public void sayIncorrectVersion(String serverVersion, String ip) {
        Window.getInstance().append("Deconnecté de " + ip + " : la version du serveur n'est pas la même que celle du client");
        JPanel p = new JPanel();
        JLabel label = new JLabel("<html><body>La version de votre client (version " + Main.VERSION + ") n'est pas la même que celle <br>du serveur (version " + serverVersion + ")</body></html>");
        p.add(label);
        JOptionPane.showMessageDialog(null, p, "Instant Messaging", JOptionPane.ERROR_MESSAGE);

    }

    public void sayAlreadyUsedNickname(String pseudo) {
        JPanel p = new JPanel();
        JLabel label = new JLabel("Le pseudo " + pseudo + " est déjà utilisé par un utilisateur");
        p.add(label);
        JOptionPane.showMessageDialog(null, p, "Instant Messaging", JOptionPane.ERROR_MESSAGE);
    }

    private void sayNewNicknameOK() {
        JPanel p = new JPanel();
        JLabel label = new JLabel("Votre pseudo a été correctement changé !");
        p.add(label);
        JOptionPane.showMessageDialog(null, p, "Instant Messaging", JOptionPane.INFORMATION_MESSAGE);
    }

    private void sayUnsupportedSystemTray() {
        JPanel p = new JPanel();
        JLabel label = new JLabel("Les notifications des messages ne sont pas disponibles sur votre système");
        p.add(label);
        JOptionPane.showMessageDialog(null, p, "Instant Messaging", JOptionPane.ERROR_MESSAGE);
    }

    public void sayServerInfo(String finalString) {
        JPanel p = new JPanel();
        JLabel label = new JLabel(finalString);
        p.add(label);
        JOptionPane.showMessageDialog(null, p, "Instant Messaging", JOptionPane.INFORMATION_MESSAGE);
    }

    public void sayUnhandledError() {
        JPanel p = new JPanel();
        JLabel label = new JLabel("Une erreur inattendue s'est produite");
        p.add(label);
        JOptionPane.showMessageDialog(null, p, "Instant Messaging", JOptionPane.ERROR_MESSAGE);
        this.dispose();
    }

    public void addNotification(String comingObject) {
        if(!isNotifSupported || this.isFocused()) return;
        this.trayIcon.displayMessage("Instant Messaging", comingObject, TrayIcon.MessageType.INFO);
    }

    public void updateTitle(String ip) {
        if(ip == null) {
            this.setTitle(NO_CONN);
        }else{
            this.setTitle(IP + ip);
        }
    }

    private void quit() {
        if(ConnectionHandler.isConnected()) {
            ConnectionHandler.getInstance().disconnect(true);
        }else{
            System.exit(0);
        }
    }

    public void append(String message) {
        this.clientDisplay.append(message + "\n");
    }

    public void clearMessageField() {
        this.messageField.setText("");
    }

    public static Window getInstance() {
        return instance;
    }

    public void setConnected() {
        this.connect.setEnabled(false);
        this.disconnect.setEnabled(true);
        this.changeUsername.setEnabled(true);
        this.serverInfos.setEnabled(true);
    }

    public void setDisconnected() {
        this.connect.setEnabled(true);
        this.disconnect.setEnabled(false);
        this.changeUsername.setEnabled(false);
        this.serverInfos.setEnabled(false);
    }
}
