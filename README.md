THIS CODE IS LICENSED UNDER CREATIVE COMMONS CC-BY-NC-ND LICENSE (see https://creativecommons.org/licenses/by-nc-nd/4.0/ for more details)

This repo contains the code of the Instant Messaging - Client side project (aka comm_client). This code belongs to BadCookie20 and nobody else. Twitter: @badcookie20 Email: badcookie2000@gmail.com

You MAY:
- read its code
- download its code
- share this repo URL

You MUST NOT:
- claim this code as yours (see license website for more information)
- copy/paste an entire piece of code without naming its author (see license website for more information)
- modify any class or file in this plugin (see license website for more information)
- use this code for commercial purposes (see license website for more information)